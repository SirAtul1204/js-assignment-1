function validate() {
  if (
    !validateEmail() ||
    !validatePassword() ||
    !validateGender() ||
    !validateRole() ||
    !validatePermissions()
  ) {
    return false;
  }
  return true;
}

function handleSubmit(event) {
  event.preventDefault();
  console.log("Button Clicked");
  if (validate()) {
    console.log("Success");
    let email = sessionStorage.getItem("email");
    let password = sessionStorage.getItem("password");
    let sex = sessionStorage.getItem("sex");
    let role = sessionStorage.getItem("role");
    let permissions = sessionStorage.getItem("permissions");
    console.log({
      email,
      password,
      sex,
      role,
      permissions,
    });
    window.open("/confirmation.html", "_self");
  }
}
