function validateRole() {
  const roles = document.getElementsByName("role");
  let isRoleChecked = false;
  let role = null;
  for (let i = 0; i < roles.length; i++) {
    if (roles[i].checked) {
      isRoleChecked = true;
      role = roles[i].value;
      break;
    }
  }

  if (!isRoleChecked) {
    alert("Please select a role");
    return false;
  }
  sessionStorage.setItem("role", role);
  return true;
}
