function validatePassword() {
  const validRE = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
  const password = document.getElementById("password").value;

  if (!password.match(validRE)) {
    alert(
      "Password must have minimum 6 characters\nPassword must have at least one digit\nPassword must have at least one lower case letter\nPassword must have at least one uppercase letter"
    );

    return false;
  }
  sessionStorage.setItem("password", password);
  return true;
}
