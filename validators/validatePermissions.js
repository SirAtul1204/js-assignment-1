function validatePermissions() {
  const permissions = document.getElementsByName("permissions");
  let permissionCount = 0;
  let permissionsSelected = [];
  permissions.forEach((permission) => {
    if (permission.checked) {
      permissionCount++;
      permissionsSelected.push(permission.value);
    }
  });

  if (permissionCount < 2) {
    alert("Select at least 2 Permissions");
    return false;
  }
  sessionStorage.setItem("permissions", permissionsSelected.join(","));
  return true;
}
