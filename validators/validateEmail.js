function validateEmail() {
  const validRE =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  const email = document.getElementById("email").value;

  if (!email.match(validRE)) {
    alert("Email is not correct!");
    return false;
  }
  sessionStorage.setItem("email", email);
  return true;
}
