function validateGender() {
  const gender = document.getElementById("sex").value;
  if (gender === "sex") {
    alert("Please select a sex");
    return false;
  }
  sessionStorage.setItem("sex", gender);
  return true;
}
