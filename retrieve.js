window.addEventListener("load", (e) => {
  const email = sessionStorage.getItem("email");
  const password = sessionStorage.getItem("password");
  const sex = sessionStorage.getItem("sex");
  const role = sessionStorage.getItem("role");
  const permissions = sessionStorage.getItem("permissions");

  document.getElementById("email").innerHTML = email;
  document.getElementById("password").innerHTML = password;
  document.getElementById("sex").innerHTML = sex;
  document.getElementById("role").innerHTML = role;
  document.getElementById("permissions").innerHTML = permissions;
});
